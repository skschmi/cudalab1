#ifndef LAB1CUDACODE_H
#define LAB1CUDACODE_H


extern void func_add_withCUDA( float *x, float *y, int sz );

extern void func_mult_withCUDA( float *x, float *y, int sz );


#endif /* LAB1CUDACODE_H */