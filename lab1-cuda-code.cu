
#define BLOCKSIZE 1024

__global__ void myParallelAdd(float *x, float *y, int sz) {

    // Get our global thread ID
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    
    //Make sure we're assigned to an index smaller than the array size.
    if(id<sz) {
        //Do the addition
        x[id] = x[id] + y[id];
    }

    return;
}

__global__ void myParallelMultiply(float *x, float *y, int sz) {

    // Get our global thread ID
    int id = blockIdx.x*blockDim.x+threadIdx.x;
    
    //Make sure we're assigned to an index smaller than the array size.
    if(id<sz) {
        //Do the multiplication
        x[id] = x[id]*y[id];
    }

    return;
}

/*
* Performs the operation x = x+y, where x,y are vectors
* Uses CUDA
*/
extern "C" void func_add_withCUDA( float *x, float *y, int sz ) {
    
    /*
    * Performing the function on the CUDA device.
    */
    
    //The amoount of memory that one array takes up
    int nbytes = sizeof(float)*sz;
    
    //Allocating the memory on the CUDA device:
    float *d_x;
    float *d_y;
    cudaMalloc(&d_x, nbytes);
    cudaMalloc(&d_y, nbytes);
    
    //Copying arrays to the CUDA device:
    cudaMemcpy( d_x, x, nbytes, cudaMemcpyHostToDevice);
    cudaMemcpy( d_y, y, nbytes, cudaMemcpyHostToDevice);
    
    //Calculating the grid size (a.k.a. the number of blocks)
	int gridsize = (int)ceil((float)sz/(float)BLOCKSIZE);
	
	// Performing the calculation on the CUDA device	
	myParallelAdd<<<gridsize,BLOCKSIZE>>>(d_x,d_y,sz);
	
	//Copying the arrays back to the host (the regular computer)
	cudaMemcpy( x, d_x, nbytes, cudaMemcpyDeviceToHost );
	
	cudaFree(d_x);
	cudaFree(d_y);
	
	return;
}



/*
* Performs the operation x = x*y, where x,y are vectors (element-wise multiplication)
* Uses CUDA
*/
extern "C" void func_mult_withCUDA( float *x, float *y, int sz ) {
    
    /*
    * Performing the function on the CUDA device.
    */
    
    //The amoount of memory that one array takes up
    int nbytes = sizeof(float)*sz;
    
    //Allocating the memory on the CUDA device:
    float *d_x;
    float *d_y;
    cudaMalloc(&d_x, nbytes);
    cudaMalloc(&d_y, nbytes);
    
    //Copying arrays to the CUDA device:
    cudaMemcpy( d_x, x, nbytes, cudaMemcpyHostToDevice);
    cudaMemcpy( d_y, y, nbytes, cudaMemcpyHostToDevice);
    
    //Calculating the grid size (a.k.a. the number of blocks)
	int gridsize = (int)ceil((float)sz/(float)BLOCKSIZE);
	
	// Performing the calculation on the CUDA device	
	myParallelMultiply<<<gridsize,BLOCKSIZE>>>(d_x,d_y,sz);
	
	//Copying the arrays back to the host (the regular computer)
	cudaMemcpy( x, d_x, nbytes, cudaMemcpyDeviceToHost );
	
	cudaFree(d_x);
	cudaFree(d_y);
	
	return;
}

