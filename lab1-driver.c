/*
Steven Schmidt
CS 6235
Lab 1, CUDA programming

Your assignment is to simply add and multiply
 two vectors to get started writing programs
 in CUDA. In the provided test (lab1-driver.cView
 in a new window), the addition and multiplication
 are coded into the functions, and the file
 (makefileView in a new window) compiles and
 links for the gradlab machines (gradlab[1-13].cs.utah.edu).
What to turn in: Your code

*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "lab1-cuda-code.h"


/*
* Performs the operation x = x+y, where x,y are vectors
*/
int func_add( float *x, float *y, int sz) {
	int i;

    //Copying the unmodified "x" into "a" for the check below.
    float *a;
	a = (float *)malloc(sizeof(float)*sz);
	if (!a){
		printf("memory allocation error\n");
		exit(-1);
	}
	memcpy(a,x,sz*(sizeof(float)));	

    //Performs the CUDA calculation, the answer going into the memory location at x
    func_add_withCUDA(x,y,sz);

    //Checking that we got the right answer	
	for(i=0; i<sz; i++) {
	    if(x[i] != a[i] + y[i]) {
			return 0;
		}
	}
		
	free(a);
	return 1;
}



/*
* Performs the operation x = x*y, where x,y are vectors (element-wise multiplication)
*/	
int func_mul ( float *x, float *y, int sz) {
	int i;
	
	//Copying the unmodified "x" into "a" for the check below.
	float *a;
	a = ( float *)malloc(sizeof(float)*sz);
	if (!a){
		printf("memory allocation error\n");
		exit(-1);
	}
	memcpy(a,x,sz*(sizeof(float)));
	
    //Performs the CUDA calculation, the answer going into the memory location at x
    func_mult_withCUDA(x,y,sz);
	
	//Checking that we got the right answer	
	for ( i=0; i<sz; i++) {
		if (x[i]!= a[i] * y[i]) {
			return 0;
		}
	}
	
	free(a);
	return 1;
}



int main() {
	
	float *a,*b;
	int j;
	int i;

	for ( j=10; j<1000000; j*=10) {
		a = (float *)malloc(sizeof(float)*j);
		b = (float *)malloc(sizeof(float)*j);

		for (i=0; i<j; i++){
			a[i] = 2;
			b[i] = 3;
		}

		if(!func_add(a,b,j)) {
			printf("failed to add, j = %d\n",j);
		}
		else {
			printf("add operation completed\n");
		}
		
		if(!func_mul(a,b,j)) {
			printf("failed to mul, j = %d\n",j);
		}
		else {
			printf("mul operation completed\n");
		}	
		
		free(a);
		free(b);
	}

	return 0;
}



