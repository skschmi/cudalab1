CC=/usr/local/cuda/bin/nvcc
INCLUDE=-I/usr/local/cuda/include

LIBDIR=-L/usr/local/cuda/lib64
LIBS=-lcudart

EXECUTABLE=addmul

$(EXECUTABLE): lab1-cuda-code.o lab1-driver.o 
	$(CC) $(INCLUDE) $(LIBDIR) $(LIBS) lab1-cuda-code.o lab1-driver.o -o $(EXECUTABLE)

lab1-driver.o: lab1-driver.c
	$(CC) $(INCLUDE) $(LIBDIR) $(LIBS) -o lab1-driver.o -c lab1-driver.c

lab1-cuda-code.o: lab1-cuda-code.h lab1-cuda-code.cu
	$(CC) $(INCLUDE) $(LIBDIR) $(LIBS) -o lab1-cuda-code.o -c lab1-cuda-code.cu

clean:
	rm *.o
	rm $(EXECUTABLE)
